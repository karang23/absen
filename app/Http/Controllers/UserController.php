<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use Auth;
use DB;
use Mail;
use Illuminate\Support\Facades\Crypt;
use DataTables;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
*/
	 public function curlfunction($url,$data){

			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'http://128.199.217.103/api/user',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS =>json_encode($data),
			  CURLOPT_HTTPHEADER => array(
				'x-gps-api-key: 85efda3c62e67941aa5ee1550c9920b6',
				'Content-Type: application/json'
			  ),
			));

			$data = curl_exec($curl);

			curl_close($curl);
			return $data;
	 	 }
	 	public function get_email(Request $request){
			if ($request->session()->get('nama') != null){
				$id = $request->id;
				$data = DB::Select("call get_email($id)");
				//dd($karyawan );
			        return ($data);

			}
        
	}
	 	public function edit(Request $request, $id){
			if ($request->session()->get('nama') != null){
				$data['nama'] = $request->session()->get('nama');
				$data['role'] = $request->session()->get('nama');
				$data1 = DB::Select("Select u.*, r.id as role_id, r.role from user u join user_role r on r.id= u.id_role where u.id=$id");
				$role =  DB::table('user_role')->get() ;

			return view('edit_user',compact("data","data1","role"));
			}else{
				return redirect('/');
			}
		}
	 
		public function proses_edit(Request $request){

			$nama = $request->nama;
			$id_role = $request->id_role;
			$email = $request->email;
			$id_user = $request->id_user;
		
			
			if ($request->session()->get('nama') != null){
				$data['nama'] = $request->session()->get('nama');
				$data['role'] = $request->session()->get('nama');
				
				$update = DB::select("update user set nama = '$nama', email ='$email', id_role=$id_role where id = $id_user");
				
			return redirect('user');
			}else{
				return redirect('/');
			}

		}
	 public function proses(Request $request){
		//dd($request);
		$url = "https://project.baki.tech/gps/api/user";
		$nama=  $request->get('nama') ;
		$email=  $request->get('email') ;
		$id_role=  $request->get('role') ;
		$pass =  $request->get('password');
		$id_karyawan =  $request->get('id_karyawan');
		$data = array(
            "nama" => $nama,
            "email" => $email,
            "password" => $pass,
            "id_role" =>$id_role
        );

		$data1=$this->curlfunction($url,$data);
		$update = DB::select("update user set id_karyawan = $id_karyawan where nama = '$nama' and email= '$email' and id_role = $id_role");

		return redirect('/user');


	}
	
		public function show (Request $request){
			if ($request->session()->get('nama') != null){
				$data['nama'] = $request->session()->get('nama');
				$data['role'] = $request->session()->get('nama');
				$role =  DB::table('user_role')->get() ;
				$karyawan = DB::Select("Select k.* from karyawan k left join user u on u.id_karyawan = k.id where u.id is null order by k.nama");

			return view('user',compact('data','role','karyawan'));
			}else{
				return redirect('/');
			}

		}
		
		public function index(Request $request){
			if ($request->session()->get('nama') != null){
					 if (session('id_role') == 1 ){
					$data1 = DB::Select("select u.id, nama, role from user u join user_role ur on u.id_role = ur.id ");
					 }else{
					$data1 = DB::Select("select u.id, nama, role from user u join user_role ur on u.id_role = ur.id where ur.id = 3 ");
					 }
					return Datatables::of($data1)
							->addIndexColumn()
							->addColumn('action', function($row){		   
									$btn = '<a href="'.url('data/user/edit/'.$row->id).'" class="edit btn btn-info btn-sm">Edit</a>
									<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteItem">Delete</a>';
									return $btn;
							})
							->rawColumns(['action'])
							->make(true);
	
			}else{
				return redirect('/');
			}
		}
		public function delete_user($id){
			DB::table('user')->where('id', $id)->delete();
			return redirect('/user');    
		}  
	
}