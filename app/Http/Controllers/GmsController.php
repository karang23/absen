<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\karyawan;
use Auth;
use DB;
use Mail;
use Illuminate\Support\Facades\Crypt;
use DataTables;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DownloadPica;

class GmsController extends Controller
{
		public function show(Request $request){
			if ($request->session()->get('nama') != null){
				$data['nama'] = $request->session()->get('nama');
				$data['role'] = $request->session()->get('role');
				//$data1 = DB::Select("Select * from jabatan");
				//$data1 = DB::table('absensi')->paginate(15);
			$id_user = session('id_user');
			//dd($id_user);
			$id_karyawan = DB::Select("Select id_karyawan from user where id = $id_user ");
			$id=$id_karyawan[0]->id_karyawan;
			//dd($id);
			if($id > 0 ){
			$data1 = DB::Select("Select d.id from karyawan k  join departemen d on k.id_departemen = d.id where k.id= $id");
			$data2 = 1;
			}else {
				$data1 = 0 ;
				$data2 = 0 ;
			}
			//dd($data1);
			//dd($data1);
			return view('Gms',compact('data','data1','data2'));
			}else{
				return redirect('/');
			}

		}	
		public function hazard(Request $request){
			if ($request->session()->get('nama') != null){
				$data['nama'] = $request->session()->get('nama');
				$data['role'] = $request->session()->get('role');
				//$data1 = DB::Select("Select * from jabatan");
				//$data1 = DB::table('absensi')->paginate(15);
				$id_user = session('id_user');
				//dd($id_user);
				$id_karyawan = DB::Select("Select id_karyawan from user where id = $id_user ");
				$id=$id_karyawan[0]->id_karyawan;
				//dd($id);
				if($id > 0 ){
				$data1 = DB::Select("Select * from karyawan k  join departemen d on k.id_departemen = d.id where k.id= $id");
				$pengawas = DB::Select("Select * from karyawan k  join departemen d on k.id_departemen = d.id join jabatan j on j.id=k.id_jabatan where jabatan like '%Supervisor%' or jabatan like '%Manager%' or jabatan like '%Direktur%'");
				$data2 = 1;
				}else {
					$data1 = 0 ;
					$data2 = 0 ;
					$pengawas =   0 ;
				}
			//dd($pengawas);
			//dd($data1);
			return view('hazard',compact('data','data1','data2','pengawas'));
			}else{
				return redirect('/');
			}

		}		
		
		public function take5_g(Request $request){
			if ($request->session()->get('nama') != null){
				$data['nama'] = $request->session()->get('nama');
				$data['role'] = $request->session()->get('nama');
				//$data1 = DB::Select("Select * from jabatan");
				$data1 = DB::Select("Select kategori from take5_m_g group by kategori");
				$data2 = DB::Select("Select * from take5_m_g");
				$id_user = session('id_user');
				$karyawan= DB::Select("Select ifnull(b.nama, a.nama) as username from user a left join karyawan b on a.id_karyawan = b.id where a.id = $id_user ");

					
				//dd($karyawan);
			return view('take5',compact("data","data1","data2","karyawan"));
			}else{
				return redirect('/');
			}
		}		
		public function take5_m(Request $request){
			if ($request->session()->get('nama') != null){
				$data['nama'] = $request->session()->get('nama');
				$data['role'] = $request->session()->get('nama');
				//$data1 = DB::Select("Select * from jabatan");
				$data1 = DB::Select("Select kategori from take5_m_mn group by kategori");
				$data2 = DB::Select("Select * from take5_m_mn");
				$id_user = session('id_user');
				$karyawan= DB::Select("Select ifnull(b.nama, a.nama) as username from user a left join karyawan b on a.id_karyawan = b.id where a.id = $id_user ");

					
				//dd($karyawan);
			return view('take5_minning',compact("data","data1","data2","karyawan"));
			}else{
				return redirect('/');
			}
		}
		public function report(Request $request){
			if ($request->session()->get('nama') != null){
				$data['nama'] = $request->session()->get('nama');
				$data['role'] = $request->session()->get('nama');
				//$data1 = DB::Select("Select * from jabatan");
			$data1 = DB::table('report_take5')->paginate(50);;
								
			//	dd($data1);
			return view('report_pica',compact("data","data1"));
			}else{
				return redirect('/');
			}
		}		
		public function cari_report(Request $request){
			if ($request->session()->get('nama') != null){
				//dd($request);
				$data['nama'] = $request->session()->get('nama');
				$data['role'] = $request->session()->get('nama');
				//$data1 = DB::Select("Select * from jabatan");
				$tgl = $request->tanggal;
				$tgl_next = $request->tanggal_next;

			$data1 = DB::table('report_take5')->whereBetween('date',[$tgl,$tgl_next] )->paginate(50);
								
			//	dd($data1);
			return view('report_pica',compact("data","data1"));
			}else{
				return redirect('/');
			}
		}
		public function sesion_tgl_pica(Request $request)
		{  
		//dd($request);
			$request->session()->forget('tgl_pica');
			$request->session()->forget('tgl_end_pica');

			$tgl = $request->tgl_pica;
			$tgl_end_pica = $request->tanggal_next;
	//	dd($tgl_end_pica);
			//Session::set('tgl', $tgl);
			$request->session()->put('tgl_pica',$tgl);
			$request->session()->put('tgl_end_pica',$tgl_end_pica);


		}

		
		public function download_harian()
		{
			
			return Excel::download(new DownloadPica(), 'absen.xlsx');
		}		
		
		public function session_tgl(Request $request)
		{  $request->session()->forget('tgl');

			$tgl = $request->tgl;
			//dd($tgl);
			//Session::set('tgl', $tgl);
			$request->session()->put('tgl',$tgl);


		}
		public function insert_g(Request $request){
			//dd($id);
		  // dd($request);
			$nama = $request->nama;
			$tanggal = $request->tgl;
			$tugas = $request->tugas;
			$value = $request->value;
			$id_item = $request->id_item;
			$id_karyawan = DB::select("select id from karyawan where nama = '$nama'");			//dd($id_karyawan);

			$id =$id_karyawan[0]->id;
			$id_user = session('id_user');

			
			if ($request->session()->get('nama') != null){
				$data['nama'] = $request->session()->get('nama');
				$data['role'] = $request->session()->get('nama');
				
			//	$update = DB::select("update karyawan set nama = '$nama', hp = '$hp', id_jabatan = $jabatan , id_departemen = $departemen, id_detail = $detail where nik_sap = $nik_sap and no_id= '$no_id'");
				for($i = 0;$i< sizeof($value);$i++)
				{
							$nilai = $value[$i];
							$item = $id_item [$i];
							//dd($nilai);
							$insert = DB::select("insert into take5_tr (id_tak5_m,value,id_karyawan,date,tugas,id_user)values('$item','$nilai',$id,'$tanggal','$tugas' ,$id_user)");
							//dd($seats);
				}
				//$insert = DB::select("insert into take5_tr (departemen)values('$Departemen')");
				

				//dd($data1);
				//return redirect('/gms')->with('Status', "Take 5 success saved");
				 return redirect('/gms')->with('success', 'Take 5 success saved');   

			}else{
				return redirect('/');
			}

		}	
		public function insert_hazard(Request $request){
			//dd($id);
			$nama = $request->nama_pelapor;
			$departemen = $request->departemen;
			$pengawas_penang_jwb = $request->pengawas;
			$tgl_penyelesain = $request->tgl_penyelesain;
			$rincian_bahaya = $request->rincian_bhy;
			$kemungkinan = $request->kemungkinan;
			$konsekuensi = $request->konsekuensi;
			$resiko = $request->resiko;
			$tindakan = $request->tindakan;
			$tutup = $request->tutup;
			$signatureJSON = $request->signatureJSON;
			$tgl_pelapor = $request->date_lapor;
			$waktu_pelaporan = $request->watu_pelapor;
			$dep_penangung_jwb = $request->dep_penangung;
			$buka = $request->buka;
					 //  dd($signatureJSON);

			$id_karyawan = DB::select("select id from karyawan where nama = '$nama'");			
			//dd($id_karyawan);

			$n_pelapor_no_id =$id_karyawan[0]->id;
			$id_user = session('id_user');

			
			if ($request->session()->get('nama') != null){
				$data['nama'] = $request->session()->get('nama');
				$data['role'] = $request->session()->get('nama');
				
			//	$update = DB::select("update karyawan set nama = '$nama', hp = '$hp', id_jabatan = $jabatan , id_departemen = $departemen, id_detail = $detail where nik_sap = $nik_sap and no_id= '$no_id'");
				
				$insert = DB::select("insert into Hazard (n_pelapor_no_id,departemen,tgl_pelaporan,waktu_pelaporan,dep_penangung_jwb,pengawas_penang_jwb,tgl_penyelesain,rincian_bahaya,kemungkinan,konsekuensi,tingkat_resiko,tindakan,buka,tutup,ttd)
												  values($n_pelapor_no_id,'$departemen','$tgl_pelapor','$waktu_pelaporan','$dep_penangung_jwb' ,'$pengawas_penang_jwb','$tgl_penyelesain','$rincian_bahaya' ,'$kemungkinan','$konsekuensi', '$resiko','$tindakan', '$buka','$tutup', '$signatureJSON')");
							//dd($seats);
				
				//$insert = DB::select("insert into take5_tr (departemen)values('$Departemen')");
				

				//dd($data1);
				//return redirect('/gms')->with('Status', "Take 5 success saved");
				 return redirect('/gms')->with('success', 'Hazard Report success saved');   

			}else{
				return redirect('/');
			}

		}		
		
		public function insert_m(Request $request){
			//dd($request);
			$nama = $request->nama;
			$tanggal = $request->tgl;
			$tugas = $request->tugas;
			$value = $request->value;
			$id_item = $request->id_item;
			$jam_kejadian = $request->jam_kejadian;
			$no_bahaya = implode("@",$request->no_bhy);
			$pengendalian = implode("@",$request->pengendalian);
		 //   dd($pengendalian);

			$id_karyawan = DB::select("select id from karyawan where nama = '$nama'");			//dd($id_karyawan);

			$id =$id_karyawan[0]->id;
			$id_user = session('id_user');

			
			if ($request->session()->get('nama') != null){
				$data['nama'] = $request->session()->get('nama');
				$data['role'] = $request->session()->get('nama');
				
			//	$update = DB::select("update karyawan set nama = '$nama', hp = '$hp', id_jabatan = $jabatan , id_departemen = $departemen, id_detail = $detail where nik_sap = $nik_sap and no_id= '$no_id'");
				for($i = 0;$i< sizeof($value);$i++)
				{
							$nilai = $value[$i];
							$item = $id_item [$i];
							//dd($nilai);
							if ( $i == 12){
								$insert = DB::select("insert into take5_mn_tr (id_tak5_m,jenis_no_bahaya,pengendalian,id_karyawan,date,tugas,id_user)values('$item','$no_bahaya','$pengendalian',$id,'$tanggal','$tugas' ,$id_user)");

							}elseif($i == 14){
								$insert = DB::select("insert into take5_mn_tr (id_tak5_m,jam_kejadian,id_karyawan,date,tugas,id_user)values('$item','$jam_kejadian',$id,'$tanggal','$tugas' ,$id_user)");

							}
							else{
							$insert = DB::select("insert into take5_mn_tr (id_tak5_m,value,id_karyawan,date,tugas,id_user)values('$item','$nilai',$id,'$tanggal','$tugas' ,$id_user)");
							//dd($seats);
							}
				}
				//$insert = DB::select("insert into take5_tr (departemen)values('$Departemen')");
				

				//dd($data1);
				//return redirect('/gms')->with('Status', "Take 5 success saved");
				 return redirect('/gms')->with('success', 'Take 5 success saved');   

			}else{
				return redirect('/');
			}

		}
		
		public function delete_departemen($id){
			DB::table('departemen')->where('id', $id)->delete();
			return redirect()->with('status', "Data Karyawan berhasil dihapus.");    
		}  
	
}