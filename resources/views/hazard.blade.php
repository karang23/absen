<!-- SmartWizard html -->

@extends('layouts.app')
@section('smartwizard')
<style>
.responsive {
  width: 100%;
  height: auto;
}
 #signature,#prev{
 width: 300px;
 height: 200px;
 }
</style>


<link type="text/css" href=" {{asset('public/css/jquery.signature.css')}}" rel="stylesheet"> 

<link href="https://cdn.jsdelivr.net/npm/smartwizard@6/dist/css/smart_wizard_all.min.css" rel="stylesheet" type="text/css" />
@stop
@section('content')
<div id="page-wrapper" class="gray-bg dashbard-1">
    <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
     
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li style="padding: 20px">
                    <span class="m-r-sm text-muted welcome-message">Welcome to GApps</span>
                </li>
              
                <li>
                    <a href="{{url('/logout')}}">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
                <!--li>
                    <a class="right-sidebar-toggle">
                        <i class="fa fa-tasks"></i>
                    </a>
                </li-->
            </ul>

        </nav>
    </div>
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox ">
					<div class="ibox-title">
						<div class="ibox-tools">
							<a class="collapse-link">
								<i class="fa fa-chevron-up"></i>
							</a>
							<a class="dropdown-toggle" data-toggle="dropdown" href="#">
								<i class="fa fa-wrench"></i>
							</a>
							<ul class="dropdown-menu dropdown-user">
								<li><a href="#" class="dropdown-item">Config option 1</a>
								</li>
								<li><a href="#" class="dropdown-item">Config option 2</a>
								</li>
							</ul>
							<a class="close-link">
								<i class="fa fa-times"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content">
						<div id="smartwizard" dir="rtl-">
							<ul class="nav nav-progress">
								<li class="nav-item">
								  <a class="nav-link" href="#step-1">
									<div class="num">1</div>
								   Hazard Reporting Flow
								  </a>
								</li>
								<li class="nav-item">
								  <a class="nav-link" href="#step-2">
									<span class="num">2</span>
									OHS	Risk Matrix
								  </a>
								</li>
								<li class="nav-item">
								  <a class="nav-link" href="#step-3">
									<span class="num">3</span>
									OHS Risk Matrix 2
								  </a>
								</li>
								<li class="nav-item">
								  <a class="nav-link " href="#step-4">
									<span class="num">4</span>
									Form Hazard
								  </a>
								</li>
							</ul>

							<div class="tab-content">
							  <div id="step-1" class="tab-pane" role="tabpanel" aria-labelledby="step-1">
									<div class="col">
										<div class="row  justify-content-center align-items-center">
											<image src="{{url('public/alur_hazard.png')}}" class="responsive">
										</div>
									</div>
							  </div>
							  <div id="step-2" class="tab-pane" role="tabpanel" aria-labelledby="step-2">
									<div class="col">
										<div class="row  justify-content-center align-items-center">
											<image src="{{url('public/ohs1.png')}}" class="responsive">
										</div>
									</div>
							  </div>
								<div id="step-3" class="tab-pane" role="tabpanel" aria-labelledby="step-3">
									<div class="col">
										<div class="row  justify-content-center align-items-center">
											<image src="{{url('public/ohs2.png')}}" class="responsive">
										</div>
									</div>
								</div>
								<div id="step-4" class="tab-pane" role="tabpanel" aria-labelledby="step-4">

								  <form id="form-4" class="row row-cols-1 ms-5 me-5 needs-validation" novalidate method="POST" action="{{url('/gms/take5_hazard')}}">
								  <div class="col">
										<h3 align="center"> Laporan Bahaya / Hazard Report</h3>
										
										@if ($data2 > 0){
											@foreach ($data1 as $data)
										<table class="table table-bordered">
											<tr>
												<td bgcolor="grey" style="color: white;" colspan="3"> Nama Pelapor</td>
												<td bgcolor="grey" style="color: white;"colspan="2"> Departemen</td>
												<td  bgcolor="grey" style="color: white;"> INX. Ref</td>
											</tr>
											<tr>
												<td colspan="3"><input class="form-control" id="nama_pelapor" name="nama_pelapor" readonly value="{{$data->nama}}"></td>
												<td colspan="2"> <input class="form-control" id="departemen" value="{{$data->departemen}}" readonly name="departemen"></td>
												<td><input class="form-control"></td>
											</tr>
											<tr>
												<td colspan="2" bgcolor="grey" style="color: white;"> Tanggal</td>
												<td> <input class="form-control" id="tgl_pelapor"  readonly  name="date_lapor" value="{{ date('Y-m-d')}}"></td>
												<td colspan="2" bgcolor="grey" style="color: white;"> Waktu</td>
												<td><input class="form-control" id="watu_pelapor" readonly  name="jam_lapor" value="{{ date('H::i')}}"></td>
											</tr>
											<tr>
												<td colspan="3"  bgcolor="grey" style="color: white;"> Dep. Penangung Jawab</td>
												<td colspan="3"  bgcolor="grey" style="color: white;"> Pengawas Penangung Jawab</td>
											</tr>
											<tr>
												<td colspan="3"><input class="form-control" name="dep_penangung"></td>
												<td colspan="3"> 
													<select class="form-control" id="pengawas" name="pengawas">
														@foreach($pengawas as $pgs)
														<option value="{{$pgs->nama}}">{{$pgs->nama}} ({{$pgs->jabatan}}) </option>
														@endforeach
													</select>
												</td>
											</tr>
											<tr>
												<td colspan="2" bgcolor="grey" style="color: white;"> Tanggal</td>
												<td colspan="4"> <input class="form-control" type="date" id="tgl_penyelesain" name="tgl_penyelesain"></td>
											</tr>
											<tr>
												<td colspan="6"  bgcolor="grey" style="color: white;"> Rincian Bahaya / Hazard Details</td>
											</tr>
											<tr>
												<td colspan="6"><textarea class="form-control" id="rincian_bhy" name="rincian_bhy"></textarea></td>
											</tr>
											<tr>
												<td colspan="6"  bgcolor="grey" style="color: white;"> Rincian Bahaya / Hazard Details</td>
											</tr>
											<tr>
												<td  bgcolor="grey" style="color: white;"> Kemungkinan</td>
												<td>
													<select class="form-control" id="kemungkinan" name="kemungkinan">
														<option value="A">A.Hampir Pasti Terjadi</option>
														<option value="B">B.Sering Terjadi</option>
														<option value="C">C.Dapat Terjadi</option>
														<option value="D">D.Kadang-Kadang</option>
														<option value="E">E.Jarang Sekali</option>
													</select>
												</td>
												<td  bgcolor="grey" style="color: white;"> Konsekuensi</td>
												<td>
													<select class="form-control" id="konsekuensi" name="konsekuensi">
														<option value="1">1.Tidak Signifikan</option>
														<option value="2">2.Ringan</option>
														<option value="3">3.Sedang</option>
														<option value="4">4.Berat</option>
														<option value="5">5.Bencana</option>
													</select>
												</td>
												<td  bgcolor="grey" style="color: white;"> Tingkat Resiko</td>
												<td><input class="form-control resiko" value='' readonly id="resiko" name="resiko"></td>
											</tr>
											<tr>
												<td colspan="6"  bgcolor="grey" style="color: white;"> Tindakan Perbaikan Langsung</td>
											</tr>
											<tr>
												<td colspan="6"><textarea class="form-control"  id="tindakan" name="tindakan"></textarea></td>
											</tr>
											<tr>
												<td colspan="6"  bgcolor="grey" style="color: white;">Status</td>
											</tr>
											<tr>
												<td  bgcolor="grey" style="color: white;"> Buka</td>
												<td colspan="2"  > <input class="form-control"  id="tutup" name="tutup"></td>
												<td  bgcolor="grey" style="color: white;"> Tutup</td>
												<td colspan="2"><input class="form-control" id="tutup" name="tutup"></td>
											</tr>
											<tr>
												<td colspan="6"  bgcolor="grey" style="color: white;">Tanda Tangan</td>
											</tr>
											<tr>

												<td  rowspan="3" ><div id="captureSignature"></div></td>
												<td rowspan="3" > <textarea id="signatureJSON" hidden name="signatureJSON" class="signatureJSON"></textarea>

															<button type="button" class="btn-info" id="clear2Button">Clear</button>

												</td>
												<td rowspan="3"> 
													<div class="col">
														<input type="checkbox" class="form-check-input" id="save-info" required>
														<label class="form-check-label" for="save-info">I agree to the terms and conditions</label>
													</div>
												</td>
											</tr>

											<tr>
											
												<td></td>
											</tr><tr>
											
												<td></td>
											<tr>
												

											

										</table>
											@endforeach
										@endif
									  </div>
									</div>
								  </form>



								</div>
							</div>

							<div class="progress">
							  <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						</div>	
					</div>	
				</div>	
			</div>
		</div>	
	</div>	
					
</div>  

    <!-- Confirm Modal -->
    <div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="confirmModalLabel">Order Placed</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            Congratulations! Your order is placed.
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="closeModal()">Ok, close and reset</button>
          </div>
        </div>
      </div>
    </div>

@endsection
@section('js')
<script src="https://cdn.jsdelivr.net/npm/smartwizard@6/dist/js/jquery.smartWizard.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('public/js/jquery.ui.touch-punch.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/js/jquery.signature.js')}}"></script>


<script type="text/javascript">
 
 // Initialize
$('#captureSignature').signature({syncField: '#signatureJSON'}); 
$('#captureSignature').signature('option', 'syncFormat', "PNG"); 

$('#clear2Button').click(function() { 
    $('#captureSignature').signature('clear'); 
}); 
 
$('#svgStyles').change(function() { 
    $('#captureSignature').signature('option', 'svgStyles', $(this).is(':checked')); 
});

$("#konsekuensi,#kemungkinan").change(function() { 
    var konsekuensi = $('#konsekuensi').val();
    var kemungkinan = $('#kemungkinan').val();
	if(kemungkinan == 'A' && konsekuensi == 1){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("T");

	}else if(kemungkinan == 'A' && konsekuensi == 2){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("T");

	}else if(kemungkinan == 'A' && konsekuensi == 3){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("E");

	}else if(kemungkinan == 'A' && konsekuensi == 4){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("E");

	}else if(kemungkinan == 'A' && konsekuensi == 5){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("E");

	}else if(kemungkinan == 'B' && konsekuensi == 1){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("S");

	}else if(kemungkinan == 'B' && konsekuensi == 2){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("T");

	}else if(kemungkinan == 'B' && konsekuensi == 3){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("T");

	}else if(kemungkinan == 'B' && konsekuensi == 4){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("E");

	}else if(kemungkinan == 'B' && konsekuensi == 5){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("E");

	}else if(kemungkinan == 'C' && konsekuensi == 1){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("R");

	}else if(kemungkinan == 'C' && konsekuensi == 2){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("S");

	}else if(kemungkinan == 'C' && konsekuensi == 3){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("T");

	}else if(kemungkinan == 'C' && konsekuensi == 4){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("E");

	}else if(kemungkinan == 'C' && konsekuensi == 5){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("E");

	}else if(kemungkinan == 'D' && konsekuensi == 1){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("R");

	}else if(kemungkinan == 'D' && konsekuensi == 2){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("R");

	}else if(kemungkinan == 'D' && konsekuensi == 3){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("S");

	}else if(kemungkinan == 'D' && konsekuensi == 4){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("T");

	}else if(kemungkinan == 'D' && konsekuensi == 5){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("E");

	}else if(kemungkinan == 'E' && konsekuensi == 1){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("R");

	}else if(kemungkinan == 'E' && konsekuensi == 2){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("R");

	}else if(kemungkinan == 'E' && konsekuensi == 3){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("S");

	}else if(kemungkinan == 'E' && konsekuensi == 4){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("T");

	}else if(kemungkinan == 'E' && konsekuensi == 5){
						console.log(kemungkinan,konsekuensi);
		$('#resiko').val("S");

	}
});

</script>

     <script type="text/javascript">

        const myModal = new bootstrap.Modal(document.getElementById('confirmModal'));


        function onCancel() { 
          // Reset wizard
          $('#smartwizard').smartWizard("reset");

          // Reset form
         // document.getElementById("form-1").reset();
         // document.getElementById("form-2").reset();
         // document.getElementById("form-3").reset();
          document.getElementById("form-4").reset();
        }

        function onConfirm() {
          let form = document.getElementById('form-4');
          if (form) {
            if (!form.checkValidity()) {
              form.classList.add('was-validated');
              $('#smartwizard').smartWizard("setState", [3], 'error');
              $("#smartwizard").smartWizard('fixHeight');
              return false;
            }
            
            $('#smartwizard').smartWizard("unsetState", [3], 'error');
          //  myModal.show();
			 var elmForm = $("#form-4");
                elmForm.submit();
                                         
          }
        }

        function closeModal() {
          // Reset wizard
          $('#smartwizard').smartWizard("reset");

          // Reset form
        //  document.getElementById("form-1").reset();
        //  document.getElementById("form-2").reset();
        //  document.getElementById("form-3").reset();
          document.getElementById("form-4").reset();

          myModal.hide();
        }

        $(function() {
            // Leave step event is used for validating the forms
            $("#smartwizard").on("leaveStep", function(e, anchorObject, currentStepIdx, nextStepIdx, stepDirection) {
                // Validate only on forward movement  
                if (stepDirection == 'forward') {
                  let form = document.getElementById('form-' + (currentStepIdx + 1));
                  if (form) {
                    if (!form.checkValidity()) {
                      form.classList.add('was-validated');
                      $('#smartwizard').smartWizard("setState", [currentStepIdx], 'error');
                      $("#smartwizard").smartWizard('fixHeight');
                      return false;
                    }
                    $('#smartwizard').smartWizard("unsetState", [currentStepIdx], 'error');
                  }
                }
            });

            // Step show event
            $("#smartwizard").on("showStep", function(e, anchorObject, stepIndex, stepDirection, stepPosition) {
                $("#prev-btn").removeClass('disabled').prop('disabled', false);
                $("#next-btn").removeClass('disabled').prop('disabled', false);
                if(stepPosition === 'first') {
                    $("#prev-btn").addClass('disabled').prop('disabled', true);
                } else if(stepPosition === 'last') {
                    $("#next-btn").addClass('disabled').prop('disabled', true);
                } else {
                    $("#prev-btn").removeClass('disabled').prop('disabled', false);
                    $("#next-btn").removeClass('disabled').prop('disabled', false);
                }

                // Get step info from Smart Wizard
                let stepInfo = $('#smartwizard').smartWizard("getStepInfo");
                $("#sw-current-step").text(stepInfo.currentStep + 1);
                $("#sw-total-step").text(stepInfo.totalSteps);

                if (stepPosition == 'last') {
                  $("#btnFinish").prop('disabled', false);
                } else {
                  $("#btnFinish").prop('disabled', true);
                }

                // Focus first name
                if (stepIndex == 1) {
                  setTimeout(() => {
                    $('#first-name').focus();
                  }, 0);
                }
            });

            // Smart Wizard
            $('#smartwizard').smartWizard({
                selected: 0,
                // autoAdjustHeight: false,
                theme: 'arrows', // basic, arrows, square, round, dots
                transition: {
                  animation:'none'
                },
                toolbar: {
                  showNextButton: true, // show/hide a Next button
                  showPreviousButton: true, // show/hide a Previous button
                  position: 'bottom', // none/ top/ both bottom
                  extraHtml: `<button class="btn btn-success" id="btnFinish" disabled onclick="onConfirm()">Complete Order</button>
                              <button class="btn btn-danger" id="btnCancel" onclick="onCancel()">Cancel</button>`
                },
                anchor: {
                    enableNavigation: true, // Enable/Disable anchor navigation 
                    enableNavigationAlways: false, // Activates all anchors clickable always
                    enableDoneState: true, // Add done state on visited steps
                    markPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                    unDoneOnBackNavigation: true, // While navigate back, done state will be cleared
                    enableDoneStateNavigation: true // Enable/Disable the done state navigation
                },
            });

            $("#state_selector").on("change", function() {
                $('#smartwizard').smartWizard("setState", [$('#step_to_style').val()], $(this).val(), !$('#is_reset').prop("checked"));
                return true;
            });

            $("#style_selector").on("change", function() {
                $('#smartwizard').smartWizard("setStyle", [$('#step_to_style').val()], $(this).val(), !$('#is_reset').prop("checked"));
                return true;
            });

        });
    </script>

@stop