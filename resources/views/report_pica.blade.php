@extends('layouts.app')

@section('content')
        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
     
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li style="padding: 20px">
                    <span class="m-r-sm text-muted welcome-message">Welcome to GApps</span>
                </li>
               
                <li>
                    <a href="{{url('/logout')}}">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
                <!--li>
                    <a class="right-sidebar-toggle">
                        <i class="fa fa-tasks"></i>
                    </a>
                </li-->
            </ul>

        </nav>
        </div>

 
		<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-12">
					<div class="ibox ">
						<div class="ibox-title">
						   <center><h4>Pica Report</h4></center>
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">
									<i class="fa fa-wrench"></i>
								</a>
								<ul class="dropdown-menu dropdown-user">
									<li><a href="#" class="dropdown-item">Config option 1</a>
									</li>
									<li><a href="#" class="dropdown-item">Config option 2</a>
									</li>
								</ul>
								<a class="close-link">
									<i class="fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							{{-- notifikasi form validasi --}}
								@if ($errors->has('file'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('file') }}</strong>
								</span>
								@endif
						 
								{{-- notifikasi sukses --}}
								@if ($sukses = Session::get('sukses'))
								<div class="alert alert-success alert-block">
									<button type="button" class="close" data-dismiss="alert">×</button> 
									<strong>{{ $sukses }}</strong>
								</div>
								@endif
						 	<a href="{{url('/gms/download')}}" class="btn btn-success my-3" target="_blank">Download</a>

						 
							<div class="table-responsive">
								<table class="table table-bordered data-table">
									<thead>
										<tr>
											<th rowspan="2">No</th>
											<th colspan="12"> Take 5 General </th>
											<th colspan="15"> Take 5 Mining </th>
											<th colspan="12">Hazard </th>

										</tr>
										<tr>
												<th>Apakah saya sehat dan siap untuk bekerja?</th>
												<th>Apakah saya sudah terlatih, kompeten, dan layak untuk melakukan tugas ini?</th>
												<th>Apakah sudah terdapat prosedur atau panduan kerja, dan apakah sudah difahami?</th>
												<th>Apakah saya memakai APD yang benar?</th>
												<th>Apakah peralatan/perlengkapan yang digunakan dalam kondisi yang baik dan</th>
												<th>Apakah saya sudah memiliki izin yang sesuai untuk melakukan pekerjaan ini?</th>
												<th>Apakah saya akan terpapar peralatan dengan pelindung yang terlepas? </th>
												<th>Apakah pekerjaan tersebut menggunakan bahan berbahaya?(seperti bahan kimia, zat radioaktif)</th>
												<th>Apakah saya akan mengisolasi peralatan tanpa prosedur?</th>
												<th>Masihkah terdapat energi yang tersimpan pada peralatan yang sudah diisolasi?</th>
												<th>Apakah saya akan bekerja pada ketinggian lebih dari 1,8 m diatas tanah tanpa adanya handrail/work platform/scaffolding?</th>
												<th>Apakah saya akan bekerja pada ruang terbatas atau atmosfer yang berbahaya?</th>
										
												<th>Apakah saya sehat dan siap untuk bekerja?</th>
												<th>Apakah saya sudah terlatih, kompeten, dan layak untuk melakukan tugas ini?</th>
												<th>Apakah sudah terdapat prosedur atau panduan kerja, dan apakah sudah difahami?</th>
												<th>Apakah saya memakai APD yang benar?</th>
												<th>Apakah peralatan/perlengkapan yang digunakan dalam kondisi yang baik dan</th>
												<th>Apakah saya sudah memiliki izin yang sesuai untuk melakukan pekerjaan ini?</th>
												<th>Apakah saya akan terpapar peralatan dengan pelindung yang terlepas? </th>
												<th>Apakah pekerjaan tersebut menggunakan bahan berbahaya?(seperti bahan kimia, zat radioaktif)</th>
												<th>Apakah saya akan mengisolasi peralatan tanpa prosedur?</th>
												<th>Masihkah terdapat energi yang tersimpan pada peralatan yang sudah diisolasi?</th>
												<th>Apakah saya akan bekerja pada ketinggian lebih dari 1,8 m diatas tanah tanpa adanya handrail/work platform/scaffolding?</th>
												<th>Apakah saya akan bekerja pada ruang terbatas atau atmosfer yang berbahaya?</th>
												<th>Gunakan Hierarki Pengendalian: Eliminasi, Subsitusi, </th>
												<th>Sudahkah anda mengkomunikasikan bahaya kepada pengawas ? </th>
												<th>Jam Kejadian</th>
												
												<th>INX REF</th>
												<th>TanggalPelaporan </th>
												<th>Waktu Pelaporan </th>
												<th>Departemen Penangung Jawab </th>
												<th>Pengawas Penangung Jawab </th>
												<th>Tanggal Penyelesaian</th>
												<th>Rincian Bahaya</th>
												<th>Kemungkinan</th>
												<th>Konsekuensi </th>
												<th>Tingkat Resiko </th>
												<th>Buka</th>
												<th>Tutup </th>
												<th>Tindakan</th>
										</tr>	
										
										
											
									</thead>
									<div class="form-group row">
											<form  method="POST" action="{{url('/gms/take5_m_filter')}}">
											
												<div class="col-sm-1">
													<label>Start Date</label>
												</div>
												<div class="col-sm-3">
													<input   class="form-control tanggal" value="{{ date('Y-m-d') }}" name="tanggal" id="tanggal" type="date" >
															
												</div>
												<div  class="col-sm-1">
													<label>End Date </label>
												</div>
												<div  class="col-sm-3">
													<input   class="form-control tanggal_next" value="{{ date('Y-m-d') }}" name="tanggal_next" id="tanggal_next" type="date" >

												</div>
												
												<div class="col-sm-2">
													<button class="btn btn-success filter"> Cari </button>
												</div>
											</form>
									</div>
									<tbody>	
										@php $no=1; @endphp

										@foreach ($data1 as $datas)
											<tr>
												<td>{{$no++}}</td>
												<td>{{$datas->A}}</td>
												<td>{{$datas->B}}</td>
												<td>{{$datas->C}}</td>
												<td>{{$datas->D}}</td>
												<td>{{$datas->E}}</td>
												<td>{{$datas->F}}</td>
												<td>{{$datas->G}}</td>
												<td>{{$datas->H}}</td>
												<td>{{$datas->I}}</td>
												<td>{{$datas->J}}</td>
												<td>{{$datas->K}}</td>
												<td>{{$datas->L}}</td>		
												<td>{{$datas->A1}}</td>
												<td>{{$datas->B1}}</td>
												<td>{{$datas->C1}}</td>
												<td>{{$datas->D1}}</td>
												<td>{{$datas->E1}}</td>
												<td>{{$datas->F1}}</td>
												<td>{{$datas->G1}}</td>
												<td>{{$datas->H1}}</td>
												<td>{{$datas->I1}}</td>
												<td>{{$datas->J1}}</td>
												<td>{{$datas->K1}}</td>
												<td>{{$datas->L1}}</td>
												<td>{{$datas->minning}}</td>
												<td>{{$datas->M1}}</td>
												<td>{{$datas->jam_kejadian}}</td>
												<td></td>
												   <td>{{$datas->tgl_pelaporan }}</td>
  <td>{{$datas->waktu_pelaporan }}</td>
   <td>{{$datas->dep_penangung_jwb }}</td>
   <td>{{$datas->pengawas_penang_jwb }}</td>
    <td>{{$datas->tgl_penyelesain }}</td>
    <td>{{$datas->rincian_bahaya }}</td>
    <td>{{$datas->kemungkinan }}</td>
    <td>{{$datas->konsekuensi }}</td>
    <td>{{$datas->tingkat_resiko }}</td>
    <td>{{$datas->buka }}</td>
    <td>{{$datas->tutup }}</td>
    <td>{{$datas->tindakan }}</td>
											
											</tr>
										@endforeach
									</tbody>
								</table>
								Halaman : {{ $data1->currentPage() }} <br/>
	Jumlah Data : {{ $data1->total() }} <br/>
	Data Per Halaman : {{ $data1->perPage() }} <br/>
		{{ $data1->links() }}

 
							</div>

						</div>
					</div>
				</div>
				</div>
			</div>
			

           <div class="footer">
                <div class="float-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> PT.GMA &copy; 2022-2023
                </div>
            </div>
        </div>

@endsection
@section('js')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
$.ajaxSetup({

				  headers : {

					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

				  }

			  });  $('#tanggal, #tanggal_next').change(function () {
			//	console.log($('.tanggal').data('column') );
				var tgl= $('.tanggal').val();
				var tgl_next= $('.tanggal_next').val();
				console.log(tgl);
				$.ajax({
						url :"{{url('/sesion_tgl_pica')}}",
						type: 'POST',
						data: {'tgl_pica':tgl,'tanggal_next':tgl_next},      
						dataType: 'json',
						success:function(r){
						}
					});
			});
		


		
</script>
@stop