@extends('layouts.app')
@section('grid')

@endsection
@section('content')
 <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
     
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li style="padding: 20px">
                    <span class="m-r-sm text-muted welcome-message">Welcome to GApps</span>
                </li>
               
                <li>
                    <a href="{{url('/logout')}}">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
                <!--li>
                    <a class="right-sidebar-toggle">
                        <i class="fa fa-tasks"></i>
                    </a>
                </li-->
            </ul>

        </nav>						

        </div>

 
		<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-12">
					<div class="ibox ">
						<div class="ibox-title">
							<div class="ibox-tools">
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
								<a class="dropdown-toggle" data-toggle="dropdown" href="#">
									<i class="fa fa-wrench"></i>
								</a>
								<ul class="dropdown-menu dropdown-user">
									<li><a href="#" class="dropdown-item">Config option 1</a>
									</li>
									<li><a href="#" class="dropdown-item">Config option 2</a>
									</li>
								</ul>
								<a class="close-link">
									<i class="fa fa-times"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							{{-- notifikasi form validasi --}}
								@if ($errors->has('file'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('file') }}</strong>
								</span>
								@endif
						 
								{{-- notifikasi sukses --}}
								@if ($sukses = Session::get('sukses'))
								<div class="alert alert-success alert-block">
									<button type="button" class="close" data-dismiss="alert">×</button> 
									<strong>{{ $sukses }}</strong>
								</div>
								@endif

							<div class="row  justify-content-center align-items-center">
								<form role="form" method="POST" action="{{url('/gms/take5_m_insert')}}" >

								<table class="table table-striped table-bordered table-hover dataTables-example">
									<tr>
										<td>Nama</td>
										<td><input class="form-control" name="nama" value="{{$karyawan[0]->username}}" readonly></td>
										<td>Tanggal</td>
										<td><input type="date" name="tgl"class="form-control" required> </td>
									</tr>
									<tr>
										<td>Tugas</td>
										<td colspan="3"> <input class="form-control" required name="tugas" placeholder="Isi dengan tugas yang dikerjakan sekarang"></td>
										
									</tr>
								</table>
									<table class="table table-striped table-bordered table-hover dataTables-example" >
										@php $noo=1 ; @endphp
										@foreach ($data1 as $datas1) 
										<thead>
											<tr>
												<th> {{$noo++}}</th>
												<th> <h3><b>{{$datas1->kategori}}</b></h3></th>
												@if ($datas1->kategori != 'Buat Perubahan' )

												<th> <h3><b>YA</b></h3></th>
												<th> <h3><b>NA</b></h3></th>
												<th> <h3><b>TIDAK</b></h3></th>
												@else
												<th colspan="3"></th>
												@endif
											</tr>
										</thead>
										<tbody>
											@foreach ($data2 as $key =>$datas2)
												@php $nos=1+$key ; @endphp

												@if($datas2->kategori == $datas1->kategori)
											<tr>
												<td></td>
												<td>
														<input name="id_item[]" type="hidden" value="{{$datas2->id}}"> {{$datas2->item}}
												</td>
												@if ($datas1->kategori == 'Buat Perubahan' )
												<td colspan='3'></td>
											<input type="hidden" class="form-control" required name="value[]{{$nos}}" value="-">
										</tbody>
										</table>
										<table class="table table-bordered table-hover" >
												
										<tbody id="parent">
													<tr bgcolor="red" style="color: white;" >
														<td ><b>Jenis Atau Nomor Bahaya</b></td>
														<td colspan='4'><b>Pengendalian</b></td>
													</tr>
													<tr >
														<td>
														<select  class='form-control' name="no_bhy[]">
															<option value="1. Peralatan bergerak dan Kendaraan">1. Peralatan bergerak dan Kendaraan</option>
															<option value="2. Jalan rusak">2. Jalan rusak</option>
															<option value="3. Terpleset/Tersandung">3. Terpleset/Tersandung</option>
															<option value="4. Jatuh/Benda jatuh">4. Jatuh/Benda jatuh</option>
															<option value="5. Orang lain / Kelompok kerja">5. Orang lain / Kelompok kerja</option>
															<option value="6. Listrik">6. Listrik</option>
															<option value="7. Mekanik">7. Mekanik</option>
															<option value="8. Kebisingan">8. Kebisingan</option>
															<option value="9. Kimia">9. Kimia</option>
															<option value="10. Ruang terbatas">10. Ruang terbatas</option>
															<option value="11. Panas / Dingin">11. Panas / Dingin</option>
															<option value="12. Tekanan">12. Tekanan</option>
															<option value="13. Proyektil">13. Proyektil</option>
															<option value="14. Titik jepit">14. Titik jepit</option>
															<option value="15. Peralatan berputar">15. Peralatan berputar</option>
															<option value="16. Penyalaan yang tidak disengaja">16. Penyalaan yang tidak disengaja</option>
															<option value="17. Radiasi">17. Radiasi</option>
															<option value="8. Debu / Asap /Gas">18. Debu / Asap /Gas</option>
															<option value="19. Runtuhan Batu / Tanah Longsor">19. Runtuhan Batu / Tanah Longsor</option>
															<option value="20. Cuaca">20. Cuaca</option>
															<option value="21. Petir">21. Petir</option>
															<option value="22. Biologis">22. Biologis</option>
															<option value="23. Tumpahan / Polusi">23. Tumpahan / Polusi</option>
															<option value="24. Api / Ledakan">>24. Api / Ledakan</option>
															<option value="25. Flora / Fauna">25. Flora / Fauna</option>
														</select>
													</td>
													<td  colspan='4'><input class='form-control'name="pengendalian[]"></td>
														
													</tr>
													<button id="btn1" class="btn btn-info"> Tambah </button>
										</table>
										<table class="table table-striped table-bordered table-hover dataTables-example" >
												@elseif ($datas2->item == 'Jam Berapa')
												<td  colspan='3'><input type="time" name="jam_kejadian"class="form-control" ></td>
												<input type="hidden" class="form-control" required name="value[]{{$nos}}" value="-">

												@elseif ($datas2->item == '-')
												<td  colspan='3'></td>
												<input type="hidden" class="form-control" required name="value[]{{$nos}}" value="-">

												@else
												
												<td><input type="radio" class="form-control" required name="value[]{{$nos}}" value="YA">{{$nos}}</td>
												<td><input type="radio" class="form-control" required name="value[]{{$nos}}" value="NA"></td>
												<td><input type="radio" class="form-control" required name="value[]{{$nos}}" value="TIDAK"></td>
												@endif
												
											</tr>
												@endif

											@endforeach
										@endforeach
									</table>
									<button type="submit" class="btn btn-success" > Save
								</form>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
			

           <div class="footer">
                <div class="float-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> PT.GMA &copy; 2022-2023
                </div>
            </div>
        </div>
		

@endsection 

@section('js')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
		<script type="text/javascript">

   $("#btn1").click(function(){
    $("#parent").append("<tr><td><select  class='form-control' name='no_bhy[]'><option value='1. Peralatan bergerak dan Kendaraan'>1. Peralatan bergerak dan Kendaraan</option><option value='2. Jalan rusak'>2. Jalan rusak</option>															<option value='3. Terpleset/Tersandung'>3. Terpleset/Tersandung</option>															<option value='4. Jatuh/Benda jatuh'>4. Jatuh/Benda jatuh</option>															<option value='5. Orang lain / Kelompok kerja'>5. Orang lain / Kelompok kerja</option>															<option value='6. Listrik'>6. Listrik</option>															<option value='7. Mekanik'>7. Mekanik</option>															<option value='8. Kebisingan'>8. Kebisingan</option>															<option value='9. Kimia'>9. Kimia</option>															<option value='10. Ruang terbatas'>10. Ruang terbatas</option>															<option value='11. Panas / Dingin'>11. Panas / Dingin</option>															<option value='12. Tekanan'>12. Tekanan</option>												<option value='13. Proyektil'>13. Proyektil</option>															<option value='14. Titik jepit'>14. Titik jepit</option>															<option value='15. Peralatan berputar'>15. Peralatan berputar</option>															<option value='16. Penyalaan yang tidak disengaja'>16. Penyalaan yang tidak disengaja</option>															<option value='17. Radiasi'>17. Radiasi</option>															<option value='8. Debu / Asap /Gas'>18. Debu / Asap /Gas</option>															<option value='19. Runtuhan Batu / Tanah Longsor'>19. Runtuhan Batu / Tanah Longsor</option>															<option value='20. Cuaca'>20. Cuaca</option>															<option value='21. Petir'>21. Petir</option>															<option value='22. Biologis'>22. Biologis</option>															<option value='23. Tumpahan / Polusi'>23. Tumpahan / Polusi</option>															<option value='24. Api / Ledakan'>>24. Api / Ledakan</option>															<option value='25. Flora / Fauna'>25. Flora / Fauna</option>														</select></td><td  colspan='4'><input class='form-control' name='pengendalian[]'></td></tr>");
  });



</script>
@stop